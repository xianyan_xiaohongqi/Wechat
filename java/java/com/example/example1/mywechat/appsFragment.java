package com.example.example1.mywechat;


import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;



public class appsFragment extends Fragment {

    private List<Apps> appsList = new ArrayList<>();

    public appsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tab01, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        initApps();
        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        Adapter adapter = new Adapter(appsList);
        recyclerView.setAdapter(adapter);
        return view;
    }

    private void initApps() {
       for(int i=0;i<=1;i++) {
           Apps apps_baidu = new Apps(i, R.drawable.baidu, "百度");
           appsList.add(apps_baidu);
           Apps apps_qq = new Apps(i, R.drawable.qq, "QQ");
           appsList.add(apps_qq);
           Apps apps_taobao = new Apps(i, R.drawable.taobao, "淘宝");
           appsList.add(apps_taobao);
           Apps apps_weibo = new Apps(i, R.drawable.weibo, "微博");
           appsList.add(apps_weibo);
           Apps apps_weixing = new Apps(i, R.drawable.weixing, "微信");
           appsList.add(apps_weixing);
           Apps apps_zhifubao = new Apps(i, R.drawable.zhifubao, "支付宝");
           appsList.add(apps_zhifubao);

       }
    }
}
